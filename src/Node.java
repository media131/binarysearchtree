import java.io.Serializable;

public class Node<T extends Comparable<T>> implements Serializable{
	protected T data;
	protected Node<T> left;
	protected Node<T> right;

	public Node(T data) {
		this.data = data;
		left = null;
		right = null;
	}

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setLeft(Node<T> link) {
		left = link;
	}

	public void setRight(Node<T> link) {
		right = link;
	}

	public Node<T> getLeft() {
		return left;
	}

	public Node<T> getRight() {
		return right;
	}
}