public interface BSTInterface<T extends Comparable<T>> {
	//traversal order
	static final int INORDER = 1;
	static final int PREORDER = 2;
	static final int POSTORDER = 3;

	boolean isEmpty();
	//Returns true if empty otherwise returns false.
	int size();
	//Returns number of elements
	boolean contains(T element);
	//Returns true if BST contains an element such that e.compareTo(element) == 0; 
	//otherwise returns false.
	boolean remove(T element);
	//Removes element e from BST such that e.compareTo(element) == 0, returns true
	// if no element exists returns false.
	T get(T element);
	//Returns element from BST such that e.compareTo(element) == 0;
	//if no element exists returns null.
	void add(T element);
	//Adds element to BST, tree retains its BST property
	int reset(int orderType);
	//Initializes current position for an iteration through this BST
	//in orderType order. Returns current number of nodes in the BST.
	T getNext(int orderType);
	//Preconditions: The BST is not empty
	//The BST has been reset for orderType
	//The BST has not been modified since the most recent reset
	//The end of orderType iteration has not been reached
	//Returns the element at the current position on this BST for orderType
	//and advances the value of the current position based on the orderType.
}