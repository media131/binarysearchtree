import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

public class Driver {
	@Test
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		BSTInterface<Food> temp = new BinarySearchTree<Food>();
		BSTInterface<Food> BinaryFood = new BinarySearchTree<Food>();
		
		temp = Deserializer(temp);
		BinaryFood = temp;
		
		String enter="x",name,ingredients,skip;		
		Food food;
		int numFood;
		
		System.out.print("Food Name (press Enter to end): ");
		name = keyboard.nextLine();
		while (!name.equals("")) {
			System.out.print("Ingredients (enter 1 line): ");
			ingredients = keyboard.nextLine();
			skip = keyboard.nextLine();

			food = new Food(name, ingredients);
			BinaryFood.add(food);

			System.out.print("Food Name (press Enter to end): ");
			name = keyboard.nextLine();
		}
		
//		System.out.println("Enter 'add', 'remove', or 'search'");
//		System.out.println("Enter 'end' to leave loop");
//		
//		while (!(enter.equalsIgnoreCase("end"))) {
//			enter = keyboard.nextLine();
//			if (enter.equalsIgnoreCase("add")) {
//				System.out.println("ADDED");
//			} else if (enter.equalsIgnoreCase("remove")) {
//				System.out.println("REMOVED");
//				
//			} else if (enter.equalsIgnoreCase("search")) {
//				System.out.println("SEARCHED");
//			}
//		}
//		System.out.println("ENDED");	
		

		
		System.out.println();
		System.out.println("Menu:");

		Serializer(BinaryFood);

		numFood = BinaryFood.reset(BinarySearchTree.INORDER);
		for (int count = 1; count <= numFood; count++) {
			System.out.println(BinaryFood.getNext(BinarySearchTree.INORDER));
		}
	}

	private static BSTInterface<Food> Deserializer(BSTInterface<Food> temp) {
		try {
			FileInputStream fin = new FileInputStream("address.ser");
			ObjectInputStream ois = new ObjectInputStream(fin);
			temp = (BSTInterface<Food>) ois.readObject();
			ois.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			temp = null;
		}
		return temp;

	}

	private static void Serializer(BSTInterface<Food> temp) {
		try {
			FileOutputStream fileOutput = new FileOutputStream("address.ser");
			ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
			objectOutput.writeObject(temp);
			objectOutput.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
