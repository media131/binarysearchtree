import java.io.Serializable;

public class Food implements Comparable<Food>, Serializable {
	String name;
	String ingredients;

	public Food() {
		name = "";
		ingredients = "";
	}

	public Food(String name, String ingredients) {
		super();
		this.name = name;
		this.ingredients = ingredients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String toString() {
		return name + "\nIngredients: " + ingredients + "\n";
	}

	public int compareTo(Food food) {
		return this.getName().compareTo(food.getName());
	}

}
