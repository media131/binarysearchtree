public class LLNode<T> {
	private LLNode<T> link;
	private T data;

	public LLNode(T data) {
		this.data = data;
		link = null;
	}

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setLink(LLNode<T> link) {
		this.link = link;
	}

	public LLNode<T> getLink() {
		return link;
	}
}