import java.util.Scanner;

public class Driver2 {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		
		System.out.println("Enter 'add', 'remove', or 'search'");
		System.out.println("Enter 'end' to leave loop");
		
		String enter="x";
		
		while (!(enter.equalsIgnoreCase("end"))) {
			enter = keyboard.nextLine();
			if (enter.equalsIgnoreCase("add")) {
				System.out.println("ADDED");
			} else if (enter.equalsIgnoreCase("remove")) {
				System.out.println("REMOVED");
			} else if (enter.equalsIgnoreCase("search")) {
				System.out.println("SEARCHED");
			}
		}
		System.out.println("ENDED");

	}

}