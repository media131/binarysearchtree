import java.io.Serializable;

public class BinarySearchTree<T extends Comparable<T>> implements BSTInterface<T>, Serializable {
	protected Node<T> root;

	boolean found;
	protected LinkedUnbndQueue<T> inOrderQueue;
	protected LinkedUnbndQueue<T> preOrderQueue;
	protected LinkedUnbndQueue<T> postOrderQueue;

	public BinarySearchTree() {
		root = null;
	}

	public boolean isEmpty() {
		return (root == null);
	}

	private int recSize(Node<T> tree) {
		if (tree == null)
			return 0;

		int leftSize = recSize(tree.getLeft());
		int rightSize = recSize(tree.getRight());
		return leftSize + rightSize + 1;
	}

	public int size() {
		return recSize(root);
	}

	public int size2() {
		int count = 0;
		if (root != null) {
			LinkedStack<Node<T>> hold = new LinkedStack<Node<T>>();
			Node<T> currNode;
			hold.push(root);
			while (!hold.isEmpty()) {
				currNode = hold.top();
				hold.pop();
				count++;
				if (currNode.getLeft() != null)
					hold.push(currNode.getLeft());
				if (currNode.getRight() != null)
					hold.push(currNode.getRight());
			}
		}
		return count;
	}

	private boolean recContains(T element, Node<T> tree) {
		if (tree == null)
			return false;
		else if (element.compareTo(tree.getData()) < 0)
			return recContains(element, tree.getLeft());
		else if (element.compareTo(tree.getData()) > 0)
			return recContains(element, tree.getRight());
		else
			return true;
	}

	public boolean contains(T element) {
		return recContains(element, root);
	}

	private T recGet(T element, Node<T> tree) {
		if (tree == null)
			return null;
		else if (element.compareTo(tree.getData()) < 0)
			return recGet(element, tree.getLeft());
		else if (element.compareTo(tree.getData()) > 0)
			return recGet(element, tree.getRight());
		else
			return tree.getData();
	}

	public T get(T element) {
		return recGet(element, root);
	}

	private Node<T> recAdd(T element, Node<T> tree) {
		if (tree == null)
			tree = new Node<T>(element);
		else if (element.compareTo(tree.getData()) <= 0)
			tree.setLeft(recAdd(element, tree.getLeft()));
		else
			tree.setRight(recAdd(element, tree.getRight()));
		return tree;
	}

	public void add(T element) {
		root = recAdd(element, root);
	}

	private T getPredecessor(Node<T> tree) {
		while (tree.getRight() != null)
			tree = tree.getRight();
		return tree.getData();
	}

	private Node<T> removeNode(Node<T> tree) {
		T data;

		if (tree.getLeft() == null)
			return tree.getRight();
		else if (tree.getRight() == null)
			return tree.getLeft();
		else {
			data = getPredecessor(tree.getLeft());
			tree.setData(data);
			tree.setLeft(recRemove(data, tree.getLeft()));
			return tree;
		}
	}

	private Node<T> recRemove(T element, Node<T> tree) {
		if (tree == null)
			found = false;
		else if (element.compareTo(tree.getData()) < 0)
			tree.setLeft(recRemove(element, tree.getLeft()));
		else if (element.compareTo(tree.getData()) > 0)
			tree.setRight(recRemove(element, tree.getRight()));
		else {
			tree = removeNode(tree);
			found = true;
		}
		return tree;
	}

	public boolean remove(T element) {
		root = recRemove(element, root);
		return found;
	}

	private void inOrder(Node<T> tree) {
		if (tree != null) {
			inOrder(tree.getLeft());
			inOrderQueue.enqueue(tree.getData());
			inOrder(tree.getRight());
		}
	}

	private void preOrder(Node<T> tree) {
		if (tree != null) {
			preOrderQueue.enqueue(tree.getData());
			preOrder(tree.getLeft());
			preOrder(tree.getRight());
		}
	}

	private void postOrder(Node<T> tree) {
		if (tree != null) {
			postOrder(tree.getLeft());
			postOrder(tree.getRight());
			postOrderQueue.enqueue(tree.getData());
		}
	}

	public int reset(int orderType) {
		int numNodes = size();

		if (orderType == INORDER) {
			inOrderQueue = new LinkedUnbndQueue<T>();
			inOrder(root);
		} else if (orderType == PREORDER) {
			preOrderQueue = new LinkedUnbndQueue<T>();
			preOrder(root);
		}
		if (orderType == POSTORDER) {
			postOrderQueue = new LinkedUnbndQueue<T>();
			postOrder(root);
		}
		return numNodes;
	}

	public T getNext(int orderType) {
		if (orderType == INORDER)
			return inOrderQueue.dequeue();
		else if (orderType == PREORDER)
			return preOrderQueue.dequeue();
		else if (orderType == POSTORDER)
			return postOrderQueue.dequeue();
		else
			return null;
	}
}